interface IMenuItem {
    name: string;
    path: string;
}

export const mainMenu: IMenuItem[] = [
    { name: 'Plant pots' , path: '/plant-pots' },
    { name: 'Ceramics' , path: '/ceramics' },
    { name: 'Tables' , path: '/tables' },
    { name: 'Chairs' , path: '/chairs' },
    { name: 'Crockery' , path: '/crockery' },
    { name: 'Tableware' , path: '/tableware' },
    { name: 'Cutlery' , path: '/cutlery' },
]
